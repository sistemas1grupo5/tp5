<?xml version = "1.0" encoding = "iso-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">

<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Codigos de distancia unitaria XML/XSLT</title>
  <link rel="StyleSheet" type="text/css" href="css/estilos.css" />
</head>

<body>
<h1>Codigos de distancia unitaria</h1>
<h2>Grupo5</h2>

<ul>
    <li>Fernandez Zabala, Francisco</li>
    <li>Galliano, Ignacio</li>
    <li>Higa, Daniel</li>
    <li>Libertini Castillo, Juan Pablo</li>
</ul>
<table border="1" cellpadding="10">
 <tr><th>(Decimal)</th><th>(Gray)</th><th>Glixon</th><th>O'Brien</th><th>Tompkins</th><th>Petherick</th><th>Libaw-Craig</th></tr>
 <xsl:for-each select="number_list/cnumber">
  <tr>
   <td><xsl:value-of select="decimal" /></td>
   <td><xsl:value-of select="gray" /></td>
   <td><xsl:value-of select="glixon" /></td>
   <td><xsl:value-of select="obrien" /></td>
   <td><xsl:value-of select="tompkins" /></td>
   <td><xsl:value-of select="petherick" /></td>
   <td><xsl:value-of select="libawcraig" /></td>
  </tr>
 </xsl:for-each>
</table>
  <p></p>
  <p>
    <a href="http://validator.w3.org/check?uri=referer"><img
      src="http://www.w3.org/Icons/valid-xml10" alt="Valid XML 1.0" height="31" width="88" /></a>
    &#160;
    <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fsistemas1.scienceontheweb.net%2Ftp5%2Fcss%2Festilos.css"><img
      src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS V&#225;lido!" height="31" width="88" /></a>
  </p>

</body>
</html>

</xsl:template>
</xsl:stylesheet>
